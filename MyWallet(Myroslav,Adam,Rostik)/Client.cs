using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWallet_Myroslav_Adam_Rostik_
{
    class Client
    {
        string c_name;
        string c_surname;
        public Client (string name, string surname)
        {
            c_name = name;
            c_surname = surname;
        }
        public string GetSurname() { return c_surname; }
    }
}
